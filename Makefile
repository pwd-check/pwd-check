#
# pwd-check - tool for testing the validity of user passwords
# Copyright (C) 2016-2017 Jaromir Capik <jaromir.capik@email.cz>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#

APPNAME=pwd-check
LIBS=-lcrypt
CC=gcc --std=c99
SBINDIR=/usr/sbin
MANDIR=/usr/share/man/man8
MANPAGE=$(APPNAME).8

$(APPNAME): $(APPNAME).c
	$(CC) -Wall -pedantic $(CFLAGS) $(LDFLAGS) -o $(APPNAME) $(APPNAME).c $(LIBS)

install:
	install -Dpm755 $(APPNAME) $(DESTDIR)/$(SBINDIR)/$(APPNAME)
	install -Dpm644 $(MANPAGE) $(DESTDIR)/$(MANDIR)/$(MANPAGE)

clean:
	rm -f $(APPNAME)
