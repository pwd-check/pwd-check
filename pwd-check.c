/*
  pwd-check - tool for testing the validity of user passwords
  Copyright (C) 2016-2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <crypt.h>
#include <shadow.h>
#include <errno.h>

#define EXIT_PASS_VALID        0
#define EXIT_PASS_INVALID      1
#define EXIT_USER_DOESNT_EXIST 2
#define EXIT_CHECK_FAILED      3


int main(int argc, char **argv)
{

  struct spwd *pwd;
  char *user;
  char *pass;
  char *hash;
  char *salt;
  char *tmp;
  int i;

  if (argc != 3) {
    printf("\nUsage: pwd-check <user> <pass>\n\n");
    return EXIT_CHECK_FAILED;
  }

  user = argv[1];
  pass = argv[2];

  // get the user's pwd hash
  errno = 0;
  hash = NULL;
  setspent();
  while ( (pwd = getspent()) ) {
    if (!strcmp (user, pwd->sp_namp)) {
      hash = pwd->sp_pwdp;
      break;
    }
  }
  if (!pwd && errno) {
    perror("shadow"); // inaccessible shadow
    return EXIT_CHECK_FAILED;
  }
  endspent();

  // got no hash? -> user doesn't exist
  if (!hash) {
    return EXIT_USER_DOESNT_EXIST;
  }

  // get the salt
  tmp = hash;
  for (i = 0; i < 3; i++) { // the 3rd '$' is the last char
    while ((*tmp != '$') && (*tmp != '\0')) tmp++;
    if (*tmp != '\0') tmp++;
  }
  salt = (char *) malloc (tmp - hash + 1);
  if (!salt) {
    perror("malloc");
    return EXIT_CHECK_FAILED;
  }
  strncpy (salt, hash, tmp - hash);
  salt[tmp - hash] = '\0';

  // compare stored and generated hash
  if (strcmp (hash, crypt(pass, salt))) {
    free(salt);
    return EXIT_PASS_INVALID;
  } else {
    free(salt);
    return EXIT_PASS_VALID;
  }

}
